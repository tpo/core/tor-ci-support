#!/usr/bin/env bash

set -e

DIR=$(dirname $(realpath -s $0))

for image in ${DIR}/images/* ; do
    echo "Building ${image} ..."
    cd $image

    buildah bud --isolation chroot -t tor-ci-${image}:latest
done
