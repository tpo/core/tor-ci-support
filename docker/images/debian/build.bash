#!/usr/bin/env bash

for version in stable testing oldstable ; do 
    echo "Building Debian ${version} ..."

    docker build --force-rm --no-cache --build-arg DEBIAN_VERSION="${version}" -t "thetorproject/tor-ci-debian:${version}" .
    docker push "thetorproject/tor-ci-debian:${version}"
done
